import React from 'react';
import styled from 'styled-components';

const Link = styled.a`
    color: #fff;
`;

const ForgotPasswordLink = () => {
    return (
        <Link target="_blank" href={process.env.REACT_APP_PASSWORD_RECOVERY_URL}>Forgot Password?</Link>
    )
};

export default ForgotPasswordLink;
