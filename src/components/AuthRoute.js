import React from 'react';
import { Redirect, Route } from 'react-router-dom';

import userCache from "../utils/userCache";

const AuthRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(props) => {
            return (
                userCache.hasValue() === true
                    ? <Component {...props} />
                    : <Redirect to='/auth/login'/>
            )
        }}/>
    )
}

export default AuthRoute;
