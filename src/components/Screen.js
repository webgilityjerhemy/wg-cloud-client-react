import React from 'react';
import styled from 'styled-components'
import {useWebgilityState} from "../utils/StateContext";
import logo from "../assets/logo.svg";


const HeaderContainer = styled.div`
  background: ${props => props.theme.background.primary};
  padding: 0.5em;
  box-shadow: 0em 0.25em 1em gray;
  div {
    display: flex;
    flex-direction: row-reverse;
  }
`;

const FooterContainer = styled.div`    
    color: #fff;
    font-size: .8em;    
    padding: 0.5em;
    background: ${props => props.theme.background.primary};
    text-align:center;
`;

const ContentContainer = styled.div`
    flex: 1;
    overflow-y:auto;
}`;

const ScreenContainer = styled.div`
  position:absolute;
  top:0;
  left:0;
  width:100%;
  height:100%;
  flex-direction:column;
  display: flex;   
`;

const Header = ({children}) => {
    const [{ auth }, dispatch] = useWebgilityState();

    return (
        <HeaderContainer>
            <img style={{height: 50}} src={logo}></img>
            <div>
            {children}
            </div>
        </HeaderContainer>
    )
}

const Content = ({children}) => {
    return (
        <ContentContainer>
            {children}
        </ContentContainer>
    )
}

const Footer = ({children}) => {
    return (
        <FooterContainer>© 2020 Webgility All Rights Reserved</FooterContainer>
    )
}


const Screen = ({children}) => {
    return (
        <ScreenContainer>
            {children}
        </ScreenContainer>
    )
}

Screen.Header = Header;
Screen.Content = Content;
Screen.Footer = Footer;

export default Screen;
