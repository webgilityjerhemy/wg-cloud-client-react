import React from 'react';
import styled from 'styled-components';


const BaseButton = styled.button`
    width: 100%;
    background-color: ${props => props.theme.background.secondary};
    display: -webkit-box;
    display: flex;
    overflow: hidden;
    margin: 0 0 10px 0;
    padding: 12px 12px;
    cursor: pointer;
    -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
    -webkit-transition: all 150ms linear;
  transition: all 150ms linear;
  text-align: center;
  white-space: nowrap;
  text-decoration: none !important;
  text-transform: none;
  text-transform: capitalize;
  color: #fff;
  border: 0 none;
  border-radius: 5px;
  font-size: 1em;
  font-weight: bold;
  line-height: 1.3;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  -webkit-box-pack: center;
          justify-content: center;
  -webkit-box-align: center;
          align-items: center;
  -webkit-box-flex: 0;
          flex: 0 0 160px;
  box-shadow: 2px 5px 10px var(--color-smoke);

    &:hover {
      -webkit-transition: all 150ms linear;
      transition: all 150ms linear;
      opacity: 0.85;
    }
    &:active {
      -webkit-transition: all 150ms linear;
      transition: all 150ms linear;
      opacity: 0.75;
    }
    &:focus {
      outline: 1px dotted #959595;
      outline-offset: -4px;
    }
`;

const Button = (props) => {
    return (
        <BaseButton onClick={props.onClick}>
            {props.children}
        </BaseButton>
    )
};

export default Button;
