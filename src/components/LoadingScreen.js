import React from 'react';
import styled from 'styled-components';

const Overlay = styled.div`
    height: 80vh;
    width: 100vh
    position: absolute;
    display: flex;
    align-items: center;
    
    flex-direction: column;
    text-align: center;
    margin: 0 auto;
    justify-content: center;
`;

const LoadingMessage = styled.div`
    font-weight: bold;
    margin: 10px 0 0 0;
    font-size 1.2em;
`;

const StyledSpinner = styled.svg`
  animation: rotate 2s linear infinite;
  margin: -10px auto;
  width: 200px;
  height: 200px;
  
  & .path {
    stroke: #5652BF;
    stroke-linecap: round;
    animation: dash 1.5s ease-in-out infinite;
  }
  
  @keyframes rotate {
    100% {
      transform: rotate(360deg);
    }
  }
  
  @keyframes dash {
    0% {
      stroke-dasharray: 1, 150;
      stroke-dashoffset: 0;
    }
    50% {
      stroke-dasharray: 90, 150;
      stroke-dashoffset: -35;
    }
    100% {
      stroke-dasharray: 90, 150;
      stroke-dashoffset: -124;
    }
  }
`;

const LoadingScreen = ({isLoading, message}) => {
    return (
        <>
            {isLoading && (
                <Overlay>
                    <StyledSpinner viewBox="0 0 50 50">
                        <circle
                            className="path"
                            cx="25"
                            cy="25"
                            r="20"
                            fill="none"
                            strokeWidth="4"
                        />
                    </StyledSpinner>
                    <LoadingMessage>
                        {message}
                    </LoadingMessage>
                </Overlay>
            )}
        </>
    )
};

LoadingScreen.defaultProps = {
    message: 'Loading...'
};

export default LoadingScreen;

