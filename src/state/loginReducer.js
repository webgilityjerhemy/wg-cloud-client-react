import userCache from "../utils/userCache";


const loginReducer = (state, action) => {
    switch (action.type) {
        case LOGIN_ACTION.LOGIN:
            userCache.save(action.value);
            return {
                ...state,
                ...action.value
            };
        case 'LOGOUT':
            userCache.delete();
            return {...state};
        default:
            return state;
    }
};

export default loginReducer;

export const LOGIN_ACTION = {
    LOGIN: 'LOGIN',
    LOGOUT: 'LOGOUT'
};
