import loginReducer from "./loginReducer";
import progressSpinnerReducer from "./progressSpinnerReducer";

const mainReducer = ({ auth, progressSpinner }, action) => ({
    auth: loginReducer(auth, action),
    progressSpinner: progressSpinnerReducer(progressSpinner, action),
});

export default mainReducer;

