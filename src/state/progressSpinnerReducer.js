const progressSpinnerReducer = (state, action) => {
    switch (action.type) {
        case 'LOADING':
            return {
                ...state,
                isLoading: true,
                message: action.value
            };
        case 'FINISH_LOADING':
            return {
                ...state,
                isLoading: false,
                message: ''
            };
        default:
            return {
                ...state,
                isLoading: false,
                message: ''
            };
    }
};

export default progressSpinnerReducer;
export const PROGRESS_SPINNER_ACTION = {
    LOADING: 'LOADING',
    FINISHED: 'FINISHED'
}
