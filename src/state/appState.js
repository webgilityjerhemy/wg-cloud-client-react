/*
This defines the state structure for the entire application.
It is not required that all properties are defined here,
it is good practice to keep this in sync with the state being
created.
 */
import userCache from "../utils/userCache";

const initialState = {
    progressSpinner: {
        isLoading: false,
        message: '',
    },
    auth: {
        firstName: '',
        lastName: '',
        email: '',
        subscriberId: '',
        token: null,
    }
};

const appState = () => {
    if(userCache.hasValue()) {
        return {
            ...initialState,
            auth: userCache.get()
        }
    }
};

export default appState;
