import React, {Suspense, lazy, useEffect} from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Login from "./pages/login/Login";
import AuthRoute from "./components/AuthRoute";
import userCache from "./utils/userCache";
import {useWebgilityState} from "./utils/StateContext";

const Home = lazy(() => import('./pages/Home'));

const App = () => {
    return (
        <Router>
            <Suspense fallback={<div>Loading...</div>}>
              <Switch>
                <Route exact path="/auth/login" component={Login}/>
                <AuthRoute path="/" component={Home}/>
              </Switch>
            </Suspense>
        </Router>
    )
};

export default App;
