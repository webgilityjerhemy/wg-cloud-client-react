const theme = {
    screen: {
        phone: '600px',
        tablet: '768px',
        desktop: '992px',
    },
    background: {
        primary: '#001d75',
        secondary: '#ff7002'
    },
    color: '#fff'
};

export default theme;

