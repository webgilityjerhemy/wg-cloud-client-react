import userCache from "./userCache";

const getDefaultOptions = () => {
    const user = userCache.get();
    let retVal = {
        headers: {
            'Content-Type': 'application/json',
        }
    };

    if(user) {
        retVal.headers.Authorization = `Bearer ${user.token}`
    }

    return retVal;

};

const httpClient = {
    post: async (url, body) => {

        const options = {
            ...getDefaultOptions(),
            method: 'POST',
            body: JSON.stringify(body)
        };

        try {
            const response = await fetch(url, options);
            return processRequest(response);
        } catch(ex) {
            return {
                error: ex
            }
        }
    },

    get: async (url, params) => {
        const options = {
            ...getDefaultOptions(),
            method: 'GET'
        };

        let getUrl = `${url}${toQueryString(params)}`;

        try {
            const response = await fetch(getUrl, options);
            return processRequest(response);
        } catch(ex) {
            return {
                error: ex
            }
        }
    }
};

const toQueryString = (params) => {
    if(!params) return '';
    if(params) {
        return '?' + Object.keys(params)
            .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
            .join('&');
    }
};

const processRequest = (response) => {
    if (response.status === 401) {
        userCache.delete();
        window.location = '/';
    } else if (response.status !== 200) {
        throw Error(`Error ${response.status}`)
    } else {
        return response.json()
    }
};

export default httpClient;
