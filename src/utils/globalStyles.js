import {createGlobalStyle} from "styled-components";

const GlobalStyle = createGlobalStyle`
    @-webkit-keyframes autofill {
    0%,100% {
        color: #fff;
        background: transparent;
    }
}

    input:-webkit-autofill,
    input:-webkit-autofill:hover, 
    input:-webkit-autofill:focus,
    textarea:-webkit-autofill,
    textarea:-webkit-autofill:hover,
    textarea:-webkit-autofill:focus,
    select:-webkit-autofill,
    select:-webkit-autofill:hover,
    select:-webkit-autofill:focus {
        -webkit-animation-delay: 1s; /* Safari support - any positive time runs instantly */
        -webkit-animation-name: autofill;
        -webkit-animation-fill-mode: both;
    }

  body {

  }
`;

export default GlobalStyle;
