const CACHE_KEY = 'wg_user';

const userCache = {
    save: (obj) => localStorage.setItem(CACHE_KEY, JSON.stringify(obj)),
    get: () => userCache.hasValue(CACHE_KEY) ? JSON.parse(localStorage.getItem(CACHE_KEY)) : null,
    hasValue: () => localStorage.getItem(CACHE_KEY) !== null,
    delete: () => localStorage.removeItem(CACHE_KEY)
};

export default userCache;
