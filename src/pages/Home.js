import React, {Suspense, lazy, useEffect} from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Screen from "../components/Screen";
import {useWebgilityState} from "../utils/StateContext";
import LoadingScreen from "../components/LoadingScreen";
import {FullStoryAPI} from "react-fullstory";

const QuickSightDashboard = lazy(() => import('./dashboard/QuickSightDashboard'));

const Home = () => {
    const [state, dispatch] = useWebgilityState();

    useEffect(() => {
        FullStoryAPI('identify', state.auth.subscriberId, {
            email: state.auth.email,
        });

    },[]);

    return (
        <Screen>
            <Screen.Header>
                {state.auth.email}
            </Screen.Header>
            <Screen.Content>
                <LoadingScreen {...state.progressSpinner}></LoadingScreen>
                <Router>
                    <Suspense fallback={<LoadingScreen />}>
                        <Switch>
                            <Route exact path="/dashboard" component={QuickSightDashboard}/>
                            <Redirect path="/" to="/dashboard"/>
                        </Switch>
                    </Suspense>
                </Router>
            </Screen.Content>
            <Screen.Footer>Test</Screen.Footer>
        </Screen>
    )
};

export default Home;
