import React, {useEffect, useRef, useState} from 'react';
import styled from 'styled-components'
import {useWebgilityState} from "../../utils/StateContext";
import httpClient from "../../utils/HttpClient";
import { embedDashboard } from 'amazon-quicksight-embedding-sdk';
import { PROGRESS_SPINNER_ACTION } from "../../state/progressSpinnerReducer";

const DashboardContainer = styled.div`
    width: 100%;
    height: 100%;
`;

const NoDataContainer = styled.div`

      height: 500px;
      position: relative;
      margin: auto;
      width: 50%;
      padding: 10px;
    
    .vertical-center {
      margin: 0;
      position: absolute;
      top: 50%;
      -ms-transform: translateY(-50%);
      transform: translateY(-50%);
    }


`;

const QuickSightDashboard = () => {
    const [state, dispatch] = useWebgilityState();

    const [dashboardUrl, setDashbardUrl] = useState();
    const [noDashboardData, setNoDashboardData] = useState(false);

    const container = useRef(null);

    useEffect( () => {
        async function getDashboard() {
            const dashboardUrl = `${process.env.REACT_APP_DASHBOARD_API}/${process.env.REACT_APP_QUICKSIGHT_DASHBOARD_ID}`;
            const result = await httpClient.get(dashboardUrl);
            if (!result.error) {
                const dashboard = embedDashboard({
                    url: result.url,
                    container: '#quicksightContainer',
                    locale: "en-US",
                    width: '100%',
                    height: '100%',
                    footerPaddingEnabled: true
                });

                dashboard.on("load", () => {
                    dispatch({type: PROGRESS_SPINNER_ACTION.FINISHED})
                });
            } else {
                setNoDashboardData(true);
                dispatch({type: PROGRESS_SPINNER_ACTION.FINISHED})
            }
        };

        dispatch({type: PROGRESS_SPINNER_ACTION.LOADING, value: 'Loading Dashboard...'});
        getDashboard();

    }, []);

    return (
        <>
            {noDashboardData && (
                <NoDataContainer>
                    <div class="vertical-center">
                        <h2>Thank you for visiting Webgility Mobile!</h2>
                        <p>
                            At this time, participation is limited for this application. If you
                            would like to know more about Webgility Mobile, including when you may
                            be eligible, please contact our exceptional Support team!
                        </p>
                    </div>
                </NoDataContainer>
            )}
            {!noDashboardData && (
                <DashboardContainer id="quicksightContainer" ref={container}></DashboardContainer>
            )}
        </>
    )
}

export default QuickSightDashboard;
