import React, {useEffect, useState} from 'react';
import styled from 'styled-components'
import { useHistory } from "react-router-dom";
import {useWebgilityState} from "../../utils/StateContext";
import httpClient from "../../utils/HttpClient";
import logo from '../../assets/logo.svg';
import Button from "../../components/Button";
import ForgotPasswordLink from "../../components/ForgotPasswordLink";
import userCache from "../../utils/userCache";
import LoadingScreen from "../../components/LoadingScreen";
import {PROGRESS_SPINNER_ACTION} from "../../state/progressSpinnerReducer";
import {LOGIN_ACTION} from "../../state/loginReducer";

const ErrorCode = styled.div`
    color: #f00;
    font-weight: bold;
    padding: 0 0 30px 0;
`;

const ModalContainer = styled.div`
    padding: 40px;
    background: ${props => props.theme.background.primary};
    box-sizing: border-box;
    text-align: center;
    height: 100vh;
    
    h2 {
      margin: 0 0 10px;
      padding: 0;
      color: #fff;
      text-align: center;
    }
    
    @media (min-width: ${props => props.theme.screen.desktop}) {
        height: auto;
        position: absolute;
        top: 50%;
        left: 50%;
        width: 400px; 
        border-radius: 10px;
        transform: translate(-50%, -50%); 
        
        -webkit-box-shadow: 10px 10px 23px 1px rgba(0,0,0,0.5);
        -moz-box-shadow: 10px 10px 23px 1px rgba(0,0,0,0.5);
        box-shadow: 10px 10px 23px 1px rgba(0,0,0,0.5);
    }
`;

const InputContainer = styled.div`
    position: relative;
`;

const Label = styled.label`
  position: absolute;
  top:0;
  left: 0;
  padding: 10px 0;
  font-size: 16px;
  color: #fff;
  pointer-events: none;
  transition: .5s;
  font-weight: normal;
`;


const Input = styled.input`
    width: 100%;
    padding: 10px 0 5px 0;
    font-size: 16px;
    color: #fff;
    margin-bottom: 30px;
    border: none;
    border-bottom: 1px solid #fff;
    outline: none;
    background: transparent;
    
    &:focus ~ ${Label},
    &:valid ~ ${Label} {
        font-weight: bold;
        top: -30px;
        left: 0;
        font-size: 12px;
    }
`;

const Login = () => {
    let history = useHistory();

    /* Inject auth property from AppState and reducers */
    const [{progressSpinner}, dispatch] = useWebgilityState();
    const [errorCode, setErrorCode] = useState('');

    const [credentials, setCredentials] = useState({
        username: undefined,
        password: undefined
    });

    useEffect(() => {
        if(userCache.hasValue()) {
            const user = userCache.get();
            dispatch({type: LOGIN_ACTION.LOGIN, value: user});
            history.push('/dashboard');
        }
    }, []);

    const handleChange = (event) => {
        setCredentials({
            ...credentials,
            [event.target.name]: event.target.value
        });
    };

    const loginRequest = async (e) => {
        e.preventDefault();

        dispatch({type: PROGRESS_SPINNER_ACTION.LOADING, value: 'Logging In...'});

        const response = await httpClient.post(process.env.REACT_APP_AUTHENTICATION_API, credentials);

        if (!response.error) {
            dispatch({type: LOGIN_ACTION.LOGIN, value: response});
            history.push('/dashboard');
        } else {
            setErrorCode(response.error.toString());
            setTimeout(() => {
                dispatch({type: PROGRESS_SPINNER_ACTION.FINISHED})
            }, 3000)
        }
    };

    return (
        <>
            <LoadingScreen {...progressSpinner}></LoadingScreen>
            {!progressSpinner.isLoading && (
                <ModalContainer>
                    <h2>
                        <img src={logo}></img>
                    </h2>
                    <form onSubmit={loginRequest}>
                        <InputContainer>
                            <Input type='text' name='username' required value={credentials.username} onChange={handleChange} />
                            <Label>Username</Label>
                        </InputContainer>
                        <InputContainer>
                            <Input type='password' name='password' required value={credentials.password} onChange={handleChange}/>
                            <Label>Password</Label>
                        </InputContainer>
                        <ErrorCode>{errorCode}</ErrorCode>
                        <Button onClick={loginRequest}>Sign In</Button>
                    </form>
                    <ForgotPasswordLink />
                </ModalContainer>
            )}
        </>
    )
}

export default Login;
