import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import GlobalStyle from "./utils/globalStyles";
import {StateProvider} from "./utils/StateContext";
import AppState from "./state/appState";
import mainReducer from "./state/reducer";
import {ThemeProvider} from "styled-components";
import theme from "./utils/theme";
import FullStory from "react-fullstory";
import ReactGA from "react-ga";

const ORG_ID = process.env.REACT_APP_FULLSTORY_ID;
const GOOGLE_ANALYTICS_ID = process.env.REACT_APP_GOOGLE_ID;

ReactGA.initialize(GOOGLE_ANALYTICS_ID);
ReactGA.pageview(window.location.pathname + window.location.search);

ReactDOM.render(
  <React.StrictMode>
      <GlobalStyle/>
      <FullStory org={ORG_ID} />
      <StateProvider initialState={AppState()} reducer={mainReducer}>
          <ThemeProvider theme={theme}>
            <App />
          </ThemeProvider>
      </StateProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
